<?php
class Ebuynow_Translation_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $scanDir;
    protected $fileExtensions;
    protected $skipAdminhtml;
    protected $locales;

    public function __construct()
    {
        $this->scanDir = Mage::getStoreConfig('dev/translation/scan_dir');
        $this->fileExtensions = Mage::getStoreConfig('dev/translation/file_extensions');
        $this->skipAdminhtml = Mage::getStoreConfig('dev/translation/skip_adminhtml');
        $this->locales = Mage::getStoreConfig('dev/translation/locales');
    }

    public function getLocales()
    {
        $locales = explode(',', $this->locales);
        return $locales;
    }

    public function getTranslationByLocale()
    {
        $translationByLocale = array();
        $phrases = $this->_getPhrases();
        $translations = $this->_getTranslations($phrases);
        for($i = 0; $i < count($phrases); $i++) {
            foreach (array_keys($translations) as $locale) {
                $translationByLocale[$i][$locale] = $translations[$locale][$i];
            }
        }
        return $translationByLocale;
    }

    private function _getTranslations($phrases)
    {
        $translations = array();
        $locales = $this->getLocales();
        $trPath = Mage::getBaseDir('media') . '/tr';
        if (!is_dir($trPath)) {
            mkdir($trPath);
        }
        $fileTpl = $trPath . '/%s.csv';
        foreach ($phrases as $phrase) {
            $translations['Orig'][] = $phrase;
        }
        foreach ($locales as $locale) {
            file_put_contents(sprintf($fileTpl, $locale),"");//flush file before inserts
            Mage::getSingleton('core/translate')->setLocale($locale)->init('frontend', true);
            foreach ($phrases as $phrase) {
                $tr = Mage::helper('core')->__($phrase);
                $translations[$locale][] = $tr;
                if ($phrase == $tr) {
                    file_put_contents(sprintf($fileTpl, $locale), $phrase . "\n", FILE_APPEND);
                }
            }
        }
        return $translations;
    }

    private function _getPhrases()
    {
        $phrases = array();
        $patterns = array(
            '#->__\(\'([^\']+)#',
            '#->__\("([^"]+)#',
        );
        $dirResult = $this->_findAllFiles($this->scanDir);
        foreach ($dirResult as $file) {
            $content = file_get_contents($file);
            $content = str_replace(array("\'", '\"'), array("~single-quote~", "~double-quote~"), $content);
            foreach ($patterns as $pattern) {
                if (preg_match_all($pattern, $content, $matches)) {
                    foreach ($matches[1] as $key => &$value) {
                        $value = str_replace(array("~single-quote~", "~double-quote~"), array("\'", '\"'), $value);
                    }
                    $phrases = array_unique(array_merge($phrases, $matches[1]));
                }
            }
        }
        return $phrases;
    }

    private function _findAllFiles($dir)
    {
        $root = scandir($dir);

        $result = array();
        $fileExtensionsPattern = $this->_getExtensionPattern();
        foreach($root as $value)
        {
            if($value === '.' || $value === '..' || $value === '.git') {continue;}
            if(is_file("$dir/$value")) {
                if (!preg_match ($fileExtensionsPattern, $value)) continue;
                if ($this->skipAdminhtml && preg_match('/adminhtml/i', $dir)) continue;
                $result[]="$dir/$value";continue;
            }
            foreach($this->_findAllFiles("$dir/$value") as $value)
            {
                $result[]=$value;
            }
        }
        return $result;
    }

    private function _getExtensionPattern()
    {
        $fileExtensions = explode('|', $this->fileExtensions);
        foreach ($fileExtensions as &$fileExtension) {
            $fileExtension = '(\.' . $fileExtension . ')';
        }
        return '/' . implode ('|', $fileExtensions) . '$/';
    }
}