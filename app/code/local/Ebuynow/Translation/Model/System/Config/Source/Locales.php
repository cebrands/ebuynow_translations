<?php

class Ebuynow_Translation_Model_System_Config_Source_Locales
{
    const PROMO                         = 'PROMO';
    const EXTENSION_UPDATE_CUSTOMER     = 'EXTENSION_UPDATE_CUSTOMER';
    const EXTENSION_UPDATE              = 'EXTENSION_UPDATE';
    const NEW_EXTENSION                 = 'NEW_EXTENSION';
    const NEWS                          = 'NEWS';


    public function toOptionArray()
    {
        return Mage::app()->getLocale()->getOptionLocales();
    }

    public function toArray()
    {
        return array(
            self::PROMO                     => 'Promotion/Discount',
            self::EXTENSION_UPDATE_CUSTOMER => 'My extensions updates',
            self::EXTENSION_UPDATE          => 'All extensions updates',
            self::NEW_EXTENSION             => 'New Extension',
            self::NEWS                      => 'Other information',
        );
    }
}
