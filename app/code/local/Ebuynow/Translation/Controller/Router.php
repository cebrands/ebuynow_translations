<?php

class Ebuynow_Translation_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{

    public function initControllerRouters($observer)
    {
        $front = $observer->getEvent()->getFront();
        $front->addRouter('tr', $this);
    }

    public function match(Zend_Controller_Request_Http $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        if ($identifier == 'tr/gen.php' ) {
            $request
                ->setControllerName('index')
                ->setActionName('generate');
            return true;
        } else {
            return false;
        }
    }
}